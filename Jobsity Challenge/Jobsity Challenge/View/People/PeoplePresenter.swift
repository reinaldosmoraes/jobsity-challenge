//
//  PeoplePresenter.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 21/03/21.
//

import Foundation
import Alamofire

class PeoplePresenter {
    
    private var view: PeopleProtocol
    private var navigator: Navigator
    
    var people: [Person?] = []
    
    init(view: PeopleProtocol, navigator: Navigator) {
        self.view = view
        self.navigator = navigator
    }
    
    func getPeople(value: String?) {
        
        guard let searchString = value else {
            self.people = []
            view.updateTableView()
            return
        }
        
        Network.shared.request(endpoint: "/search/people", method: .get, header: nil, parameters: ["q": searchString]) { (result) in
            if result.isSuccess {
                do {
                    guard let data = result.value else { return }
                    let response = try JSONDecoder().decode([PeopleSearch?].self, from: data)
                    self.people = response.map({ (peopleSearch) -> Person? in
                        return peopleSearch?.person
                    })
                    self.view.updateTableView()
                } catch {
                    print(error.localizedDescription)
                }
            } else {
                print(result.error ?? "")
            }
        }
    }
    
    func navigateToPersonDetailsViewController(person: Person) {
        navigator.navigateToPersonDetailsViewController(person: person)
    }
    
}
