//
//  PersonDetailsProtocol.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 21/03/21.
//

import Foundation

protocol PersonDetailsProtocol {
    func updateCellectionView()
}
