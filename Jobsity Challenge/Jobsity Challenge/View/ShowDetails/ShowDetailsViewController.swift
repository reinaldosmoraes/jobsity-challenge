//
//  ShowDetailsViewController.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 20/03/21.
//

import Foundation
import UIKit
import Kingfisher

class ShowDetailsViewController: UIViewController, ShowDetailsProtocol {
    
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var scheduleStackView: UIStackView!
    @IBOutlet weak var scheduleIcon: UIImageView!
    
    @IBAction func seeEpisodesButtonAction(_ sender: Any) {
        guard let showId = show?.id else { return }
        presenter?.navigateToEpisodesViewController(showId: showId)
    }
    
    private var presenter: ShowDetailsPresenter?
    var show: Show?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ShowDetailsPresenter(view: self, navigator: Navigator(viewContoller: self))
        updateView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureFavoriteButton()
    }
    
    override func viewDidLayoutSubviews() {
        posterImage.addDefaultGradient()
    }
    
    func configureFavoriteButton() {
        let favoriteTitle = presenter?.isFavoriteShow(showId: show?.id) ?? false ? "Remove from favorites" : "Add to favorites"
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: favoriteTitle, style: .plain, target: self, action: #selector(addFavoriteShow))
    }
    
    func updateView() {
        
        guard let show = show else { return }
        
        nameLabel.text = show.name
        genresLabel.text = show.genres?.joined(separator: ", ")
        
        if show.schedule?.time == "" && show.schedule?.days?.count == 0 {
            scheduleStackView.isHidden = true
            scheduleIcon.isHidden = true
        }
        
        if let time = show.schedule?.time, time != "" {
            timeLabel.text = time
        } else {
            timeLabel.isHidden = true
        }
        
        if let days = show.schedule?.days, days.count > 0 {
            daysLabel.text = days.joined(separator: ", ")
        } else {
            daysLabel.isHidden = true
        }
        
        summaryLabel.text = show.summary?.htmlToString
        if let imageUrl = show.image?.original {
            posterImage.kf.setImage(with: URL(string: imageUrl))
        } else {
            posterImage.image = UIImage(named: "image_placeholder")
        }
    }
    
    @objc func addFavoriteShow() {
        if presenter?.isFavoriteShow(showId: show?.id) == true {
            presenter?.removeShowFromFavorites(showId: show?.id)
            configureFavoriteButton()
        } else {
            presenter?.addShowToFavorites(show: show)
            configureFavoriteButton()
        }
    }
}
