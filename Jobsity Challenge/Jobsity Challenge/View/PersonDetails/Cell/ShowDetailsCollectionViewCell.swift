//
//  ShowDetailsCollectionViewCell.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 22/03/21.
//

import UIKit

class ShowDetailsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
}
