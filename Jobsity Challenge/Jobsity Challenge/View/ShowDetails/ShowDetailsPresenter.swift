//
//  ShowDetailsPresenter.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 20/03/21.
//

import Foundation

class ShowDetailsPresenter {
    
    private var view: ShowDetailsProtocol
    private var navigator: Navigator
    
    init(view: ShowDetailsProtocol, navigator: Navigator) {
        self.view = view
        self.navigator = navigator
    }
    
    func isFavoriteShow(showId: Int?) -> Bool {
        guard let id = showId else { return false }
        return StorageHelper.shared.isFavoriteShow(showId: id)
    }
    
    func addShowToFavorites(show: Show?) {
        guard let show = show else { return }
        StorageHelper.shared.addShowToFavorites(show: show)
    }
    
    func removeShowFromFavorites(showId: Int?) {
        guard let id = showId else { return }
        StorageHelper.shared.removeShowFromFavorites(showId: id)
    }
    
    func navigateToEpisodesViewController(showId: Int) {
        navigator.navigateToEpisodesViewController(showId: showId)
    }
    
}
