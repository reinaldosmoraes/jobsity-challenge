//
//  FavoritesCell.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 22/03/21.
//

import UIKit
import Cosmos

class FavoritesCell: UITableViewCell {
    
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    
    override func awakeFromNib() {
        ratingView.settings.updateOnTouch = false
        ratingView.settings.fillMode = .precise
    }
}
