//
//  Episode.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 20/03/21.
//

import Foundation

struct Episode: Codable {
    var id: Int?
    var url: String?
    var name: String?
    var season: Int?
    var number: Int?
    var type: String?
    var airdate: String?
    var airtime: String?
    var airstamp: String?
    var runtime: Int?
    var image: ImageResponse?
    var summary: String?
    var links: LinksResponse?
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case url = "url"
        case name = "name"
        case season = "season"
        case number = "number"
        case type = "type"
        case airdate = "airdate"
        case airtime = "airtime"
        case airstamp = "airstamp"
        case runtime = "runtime"
        case image = "image"
        case summary = "summary"
        case links = "_links"
    }
}
