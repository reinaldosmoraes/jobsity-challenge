//
//  FavoritesProtocol.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 22/03/21.
//

import Foundation

protocol FavoritesProtocol {
    func updateTableView()
}
