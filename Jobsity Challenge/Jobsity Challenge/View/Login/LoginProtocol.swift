//
//  LoginProtocol.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 23/03/21.
//

import Foundation

protocol LoginProtocol {
    func showIncorrectPasswordError()
}
