//
//  MainPresenter.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 20/03/21.
//

import Foundation
import Alamofire

class MainPresenter {
    
    private var view: MainProtocol
    private var navigator: Navigator
    
    private var page = 0
    private var isPaginating = false
    private var isOnSearchMode = false
    
    var shows: [Show?] = []
    
    init(view: MainProtocol, navigator: Navigator) {
        self.view = view
        self.navigator = navigator
    }
    
    func getSeries(value: String?) {
        isOnSearchMode = true
        
        guard let searchString = value else {
            self.shows = []
            view.updateTableView()
            return
        }
        
        Network.shared.request(endpoint: "/search/shows", method: .get, header: nil, parameters: ["q": searchString]) { (result) in
            if result.isSuccess {
                do {
                    guard let data = result.value else { return }
                    let response = try JSONDecoder().decode([ShowSearch?].self, from: data)
                    self.shows = response.map({ (showSearch) -> Show? in
                        return showSearch?.show
                    })
                    self.view.updateTableView()
                    self.resetPagination()
                } catch {
                    print(error.localizedDescription)
                }
            } else {
                print(result.error ?? "")
            }
        }
    }
    
    func getAllShows() {
        isOnSearchMode = false
        isPaginating = true
        Network.shared.request(endpoint: "/shows", method: .get, header: nil, parameters: ["page": page]) { (result) in
            if result.isSuccess {
                do {
                    guard let data = result.value else { return }
                    let response = try JSONDecoder().decode([Show?].self, from: data)
                    
                    if self.page == 0 {
                        self.shows = response
                    } else {
                        self.shows.append(contentsOf: response)
                    }
                    self.view.updateTableView()
                    self.page += 1
                } catch {
                    print(error.localizedDescription)
                }
            } else {
                print(result.error ?? "")
            }
            self.isPaginating = false
        }
    }
    
    func loadMoreDataIfNeeded(row: Int) {
        if row == (shows.count - 2) && !isOnSearchMode {
            getAllShows()
        }
    }
    
    func resetPagination() {
        page = 0
    }
    
    func navigateToShowDetailsViewController(show: Show) {
        navigator.navigateToShowDetailsViewController(show: show)
    }
    
}
