//
//  ShowDetailsCell.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 20/03/21.
//

import UIKit
import Cosmos

class ShowDetailsCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var ratingView: CosmosView!
 
    override func awakeFromNib() {
        ratingView.settings.updateOnTouch = false
        ratingView.settings.fillMode = .precise
    }
}
