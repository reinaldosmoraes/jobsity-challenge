//
//  Network.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 20/03/21.
//

import Foundation
import Alamofire

class Network {
    
    static let shared = Network()
    let baseURL = "http://api.tvmaze.com"
    
    func request(endpoint: String, method: HTTPMethod, header: HTTPHeaders?, parameters: [String : Any]?, completion: @escaping (_ result: Result<Data>) -> Void) {
        
        let url = baseURL + endpoint
        
        Alamofire.request(url,
                          method: .get,
                          parameters: parameters,
                          headers: header)
            .validate()
            .responseData { (response: DataResponse <Data>) in
                let result = response.result
                completion(result)
        }
    }
    
}
