//
//  People.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 21/03/21.
//

import Foundation

struct PeopleSearch: Codable {
    var score: Double?
    var person: Person?
    
    private enum CodingKeys: String, CodingKey {
        case score = "score"
        case person = "person"
    }
}

struct Person: Codable {
    var id: Int?
    var url: String?
    var name: String?
    var country: Country?
    var birthday: String?
    var gender: String?
    var image: ImageResponse?
    var links: LinksResponse?
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case url = "url"
        case name = "name"
        case country = "country"
        case birthday = "birthday"
        case gender = "gender"
        case image = "image"
        case links = "_links"
    }
}

struct Country: Codable {
    var name: String?
    var code: String?
    var timezone: String?
    
    private enum CodingKeys: String, CodingKey {
        case name = "name"
        case code = "code"
        case timezone = "timezone"
    }
}

struct PersonCastCredits: Codable {
    var voice: Bool?
    var embedded: Embedded?
    
    private enum CodingKeys: String, CodingKey {
        case voice = "voice"
        case embedded = "_embedded"
    }
}

struct Embedded: Codable {
    var show: Show?
    
    private enum CodingKeys: String, CodingKey {
        case show = "show"
    }
}

