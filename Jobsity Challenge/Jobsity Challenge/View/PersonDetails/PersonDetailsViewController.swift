//
//  PersonDetailsViewController.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 21/03/21.
//

import Foundation
import UIKit

class PersonDetailsViewController: UIViewController {
    
    private var presenter: PersonDetailsPresenter?
    var person: Person?
    
    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = PersonDetailsPresenter(view: self, navigator: Navigator(viewContoller: self))
        presenter?.getPersonInfo(personId: person?.id)
        updateUI()
    }
    
    func updateUI() {
        self.title = person?.name
        if let image = person?.image?.original {
            personImage.kf.setImage(with: URL(string: image))
        } else {
            personImage.image = UIImage(named: "image_placeholder")
        }
    }
}

extension PersonDetailsViewController: PersonDetailsProtocol {
    func updateCellectionView() {
        collectionView.reloadData()
    }
}

extension PersonDetailsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.shows.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShowDetailsCollectionViewCell", for: indexPath) as! ShowDetailsCollectionViewCell
        cell.nameLabel.text = presenter?.shows[indexPath.row]?.name
        
        if let imageUrl = presenter?.shows[indexPath.row]?.image?.original {
            cell.posterImage.kf.setImage(with: URL(string: imageUrl))
        } else {
            cell.posterImage.image = UIImage(named: "image_placeholder")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.navigateToShowDetailsViewController(show: presenter?.shows[indexPath.row])
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
}
