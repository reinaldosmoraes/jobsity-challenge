[![Platform](https://img.shields.io/cocoapods/p/LFAlertController.svg?style=flat)](http://cocoapods.org/pods/LFAlertController)


# Jobsity's iOS Coding Challenge

## Introduction

This project is an application for listing TV series, using the API provided by the [TVMaze](https://www.tvmaze.com/api) website.

The project was developed using MVP architecture with swift 5.

The application has a login screen that asks for a PIN number to access all its features. The PIN number is "2021".

You can check the dark mode version if your simulator is on dark mode and you can remove series from your favorites list swiping left items on the list.

## Mandatory features implemented

- List all of the series contained in the API used by the paging scheme provided by the API. 
- Allow users to search series by name. 
- The listing and search views must show at least the name and poster image of the series. 
-  After clicking on a series, the application should show the details of the series, showing the following information: 
    - Name
    - Poster 
    - Days and time during which the series airs 
    -  Genres 
    - Summary 
    - List of episodes separated by season 
- After clicking on an episode, the application should show the episode’s information, including: 
    - Name 
    - Number 
    - Season 
    - Summary 
    - Image, if there is one 

## Bonus (Optional) features implemented
- Allow the user to set a PIN number to secure the application and prevent unauthorised users.
- Allow the user to save a series as favorite
- Allow the user to delete a series from the favorites list. 
- Allow the user to browse their favorite series in alphabetical order, and click on one to see its details 
- Create a people search by listing the name and image of the person. 
- After clicking on a person, the application should show the details of that person, such as: 
    - Name 
    - Image 
    -  Series they have participated in, with a link to the series details. 

## Requirements

- iOS 13.0+
- Xcode 12.3+
- Cocoapods

## Dependencies

- Alamofire: HTTP networking library used on API calls
- Kingfisher: library for downloading and caching images used to download series and people images
- Cosmos: UI control to shows star ratings used to show ratings. 

## Installation

- Clone the repository on [https://bitbucket.org/reinaldosmoraes/jobsity-challenge/src/master/](https://bitbucket.org/reinaldosmoraes/jobsity-challenge/src/master/)
- Make sure you have cocoapods installed
```
$ pod --version
```
- Run the following commands on terminal to install pods and open the project on Xcode:
```
$ cd jobsity-challenge/Jobsity\ Challenge/
$ pod install
$ open Jobsity\ Challenge.xcworkspace
```
- Run project
