//
//  SeriesSearch.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 20/03/21.
//

import Foundation

struct ShowSearch: Codable {
    var score: Double?
    var show: Show?
    
    private enum CodingKeys: String, CodingKey {
        case score = "score"
        case show = "show"
    }
}

struct Show: Codable {
    var id: Int?
    var url: String?
    var name: String?
    var type: String?
    var language: String?
    var genres: [String]?
    var status: String?
    var runtime: Int?
    var premiered: String?
    var officialSite: String?
    var schedule: ScheduleResponse?
    var rating: RatingResponse?
    var weight: Int?
    var image: ImageResponse?
    var summary: String?
    var links: LinksResponse?
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case url = "url"
        case name = "name"
        case type = "type"
        case language = "language"
        case genres = "genres"
        case status = "status"
        case runtime = "runtime"
        case premiered = "premiered"
        case officialSite = "officialSite"
        case schedule = "schedule"
        case rating = "rating"
        case weight = "weight"
        case image = "image"
        case summary = "summary"
        case links = "_links"
    }
}

struct ScheduleResponse: Codable {
    var time: String?
    var days: [String]?
    
    private enum CodingKeys: String, CodingKey {
        case time = "time"
        case days = "days"
    }
}

struct RatingResponse: Codable {
    var average: Double?
    
    private enum CodingKeys: String, CodingKey {
        case average = "average"
    }
}

struct ImageResponse: Codable {
    var medium: String?
    var original: String?
    
    private enum CodingKeys: String, CodingKey {
        case medium = "medium"
        case original = "original"
    }
}

struct LinksResponse: Codable {
    var selfEpisode: HrefResponse?
    var previousEpisode: HrefResponse?
    
    private enum CodingKeys: String, CodingKey {
        case selfEpisode = "self"
        case previousEpisode = "previousepisode"
    }
}

struct HrefResponse: Codable {
    var href: String?
    
    private enum CodingKeys: String, CodingKey {
        case href = "href"
    }
}
