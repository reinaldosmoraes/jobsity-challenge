//
//  EpisodesViewController.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 20/03/21.
//

import Foundation
import UIKit

class EpisodesViewController: UITableViewController {
    
    private var presenter: EpisodesPresenter?
    var showId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = EpisodesPresenter(view: self, navigator: Navigator(viewContoller: self))
        presenter?.getEposidesInfo(showId: showId)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.orderedEpisodes[section]?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = presenter?.orderedEpisodes[indexPath.section]?[indexPath.row]?.name
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return presenter?.orderedEpisodes.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Season " + String(section + 1)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let selectedEpisode = presenter?.orderedEpisodes[indexPath.section]?[indexPath.row] else { return }
        presenter?.navigateToEpisodeDetailsViewController(episode: selectedEpisode)
    }

}

extension EpisodesViewController: EpisodesProtocol {
    func updateTableView() {
        tableView.reloadData()
    }
}
