//
//  MainViewController.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 20/03/21.
//

import UIKit
import Kingfisher

class MainViewController: UITableViewController {

    private var presenter: MainPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = MainPresenter(view: self, navigator: Navigator(viewContoller: self))
        configureSearchController()
        presenter?.getAllShows()
    }
    
    private func configureSearchController() {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search TV series by name"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.shows.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShowDetailsCell", for: indexPath) as! ShowDetailsCell
        cell.nameLabel.text = presenter?.shows[indexPath.row]?.name
        
        //update image while loading the new one
        cell.posterImage.image = UIImage(named: "image_placeholder")
        
        if let rating = presenter?.shows[indexPath.row]?.rating?.average {
            cell.ratingView.rating = rating / 2.0
            cell.ratingView.isHidden = false
        } else {
            cell.ratingView.isHidden = true
        }
        
        if let imageUrl = presenter?.shows[indexPath.row]?.image?.original {
            cell.posterImage.kf.setImage(with: URL(string: imageUrl))
        }
        
        presenter?.loadMoreDataIfNeeded(row: indexPath.row)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let show = presenter?.shows[indexPath.row] else { return }
        presenter?.navigateToShowDetailsViewController(show: show)
    }
    
}

extension MainViewController: MainProtocol {
    func updateTableView() {
        tableView.reloadData()
    }
}

extension MainViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text?.count ?? 0 > 1 {
            presenter?.getSeries(value: searchController.searchBar.text)
        }
    }
}
