//
//  EpisodesPresenter.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 20/03/21.
//

import Foundation

class EpisodesPresenter {
    
    private var view: EpisodesProtocol
    private var navigator: Navigator
    
    var episodes: [Episode?] = []
    var orderedEpisodes: [Int: [Episode?]] = [:]
    
    init(view: EpisodesProtocol, navigator: Navigator) {
        self.view = view
        self.navigator = navigator
    }
    
    func getEposidesInfo(showId: Int?) {
        guard let id = showId else { return }
        
        Network.shared.request(endpoint: "/shows/" + String(id) + "/episodes", method: .get, header: nil, parameters: nil) { (result) in
            if result.isSuccess {
                do {
                    guard let data = result.value else { return }
                    let response = try JSONDecoder().decode([Episode?].self, from: data)
                    self.episodes = response
                    self.orderEpisodes()
                    self.view.updateTableView()
                } catch {
                    print(error.localizedDescription)
                }
            } else {
                print(result.error ?? "")
            }
        }
    }
    
    func orderEpisodes() {
        
        for episode in episodes {
            if orderedEpisodes[(episode?.season)! - 1] == nil {
                orderedEpisodes[(episode?.season)! - 1] = []
            }
            orderedEpisodes[(episode?.season)! - 1]?.append(episode)
        }
    }
    
    func navigateToEpisodeDetailsViewController(episode: Episode) {
        navigator.navigateToEpisodeDetailsViewController(episode: episode)
    }
}
