//
//  Navigator.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 20/03/21.
//

import Foundation
import UIKit

class Navigator {
    private let currentView: UIViewController
    private let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    init(viewContoller: UIViewController) {
        self.currentView = viewContoller
    }
    
    func navigateToShowDetailsViewController(show: Show) {
        let newViewController = mainStoryboard.instantiateViewController(withIdentifier: "ShowDetailsViewController") as! ShowDetailsViewController
        newViewController.show = show
        currentView.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    func navigateToEpisodesViewController(showId: Int) {
        let newViewController = mainStoryboard.instantiateViewController(withIdentifier: "EpisodesViewController") as! EpisodesViewController
        newViewController.showId = showId
        currentView.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    func navigateToEpisodeDetailsViewController(episode: Episode) {
        let newViewController = mainStoryboard.instantiateViewController(withIdentifier: "EpisodeDetailsViewController") as! EpisodeDetailsViewController
        newViewController.episode = episode
        currentView.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    func navigateToPersonDetailsViewController(person: Person) {
        let newViewController = mainStoryboard.instantiateViewController(withIdentifier: "PersonDetailsViewController") as! PersonDetailsViewController
        newViewController.person = person
        currentView.navigationController?.pushViewController(newViewController, animated: true)
    }
    
    func navigateToTabBarController() {
        let newViewController = mainStoryboard.instantiateViewController(withIdentifier: "TabBarController")
        currentView.present(newViewController, animated: true, completion: nil)
    }
}
