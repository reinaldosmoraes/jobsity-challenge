//
//  FavoritesViewController.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 22/03/21.
//

import Foundation
import UIKit

class FavoritesViewController: UITableViewController {
    
    private var presenter: FavoritesPresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.titleView = UIView()
        self.title = "Favorites"
        presenter = FavoritesPresenter(view: self, navigator: Navigator(viewContoller: self))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter?.getFavoriteShows()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavoritesCell", for: indexPath) as! FavoritesCell
        cell.nameLabel.text = presenter?.shows[indexPath.row]?.name
        cell.genresLabel.text = presenter?.shows[indexPath.row]?.genres?.joined(separator: ", ")
        
        if let rating = presenter?.shows[indexPath.row]?.rating?.average {
            cell.ratingView.rating = rating / 2.0
            cell.ratingView.isHidden = false
        } else {
            cell.ratingView.isHidden = true
        }
        
        if let imageUrl = presenter?.shows[indexPath.row]?.image?.original {
            cell.posterImage.kf.setImage(with: URL(string: imageUrl))
        } else {
            cell.posterImage.image = UIImage(named: "image_placeholder")
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.shows.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let show = presenter?.shows[indexPath.row] {
            presenter?.navigateToShowDetailsViewController(show: show)
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, view, completion) in
            self.presenter?.removeShowFromFavorites(index: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
}

extension FavoritesViewController: FavoritesProtocol {
    func updateTableView() {
        tableView.reloadData()
    }
}
