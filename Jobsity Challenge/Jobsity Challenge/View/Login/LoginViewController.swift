//
//  LoginViewController.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 23/03/21.
//

import Foundation
import UIKit

class LoginViewController: UIViewController {
    
    private var presenter: LoginPresenter?
    var passwordTextField: UITextField?
    
    @IBAction func goToAppButtonAction(_ sender: Any) {
        showPasswordAlert()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = LoginPresenter(view: self, navigator: Navigator(viewContoller: self))
    }
    
    func showPasswordAlert() {
        let alert = UIAlertController(title: "Password", message: "The password is on README file", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.keyboardType = .numberPad
            self.passwordTextField = textField
        }
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            self.presenter?.navigateToTabBarViewController(password: self.passwordTextField?.text)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel))
        self.present(alert, animated: true, completion: nil)
    }
}

extension LoginViewController: LoginProtocol {
    func showIncorrectPasswordError() {
        let alert = UIAlertController(title: "Error", message: "Incorrect password. Check README file to get the correct password", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
