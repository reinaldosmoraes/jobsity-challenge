//
//  StorageHelper.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 22/03/21.
//

import Foundation

class StorageHelper {
    
    static let shared = StorageHelper()
    private var base: UserDefaults { return UserDefaults.standard }

    let SHOW_LIST_KEY = "SHOW_LIST_KEY"
    
    var shows: [Show?] {
        set {
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(newValue) {
                base.set(encoded, forKey: SHOW_LIST_KEY)
            } else {
                base.set(nil, forKey: SHOW_LIST_KEY)
            }
        }
        get {
            if let currentUserData = UserDefaults.standard.object(forKey: SHOW_LIST_KEY) as? Data {
                let decoder = JSONDecoder()
                if let receiptRequest = try? decoder.decode([Show?].self, from: currentUserData) {
                    return receiptRequest
                }
            }
            return []
        }
    }
    
    func addShowToFavorites(show: Show) {
        shows.append(show)
        shows = shows.sorted(by: { $0?.name ?? "" < $1?.name ?? ""})
    }
    
    func removeShowFromFavorites(showId: Int) {
        for (index, show) in shows.enumerated() {
            if showId == show?.id {
                shows.remove(at: index)
            }
        }
    }
    
    func isFavoriteShow(showId: Int) -> Bool {
        for show in shows {
            if showId == show?.id {
                return true
            }
        }
        return false
    }
    
}
