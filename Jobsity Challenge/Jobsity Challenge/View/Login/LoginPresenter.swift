//
//  LoginPresenter.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 23/03/21.
//

import Foundation

class LoginPresenter {
    
    private var view: LoginProtocol
    private var navigator: Navigator
    private let correctPassword = "2021"
    
    init(view: LoginProtocol, navigator: Navigator) {
        self.view = view
        self.navigator = navigator
    }
    
    func navigateToTabBarViewController(password: String?) {
        if password == correctPassword {
            navigator.navigateToTabBarController()
        } else {
            self.view.showIncorrectPasswordError()
        }
    }
    
}

