//
//  FavoritesPresenter.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 22/03/21.
//

import Foundation

class FavoritesPresenter {
    
    private var view: FavoritesProtocol
    private var navigator: Navigator
    
    var shows: [Show?] = []
    
    init(view: FavoritesProtocol, navigator: Navigator) {
        self.view = view
        self.navigator = navigator
    }
    
    func getFavoriteShows() {
        shows = StorageHelper.shared.shows
        self.view.updateTableView()
    }
    
    func removeShowFromFavorites(showId: Int?) {
        guard let id = showId else { return }
        StorageHelper.shared.removeShowFromFavorites(showId: id)
    }
    
    func removeShowFromFavorites(index: Int) {
        guard let id = shows[index]?.id else { return }
        StorageHelper.shared.removeShowFromFavorites(showId: id)
        shows.remove(at: index)
    }
    
    func navigateToShowDetailsViewController(show: Show?) {
        guard let show = show else { return }
        navigator.navigateToShowDetailsViewController(show: show)
    }
    
}

