//
//  PersonDetailsPresenter.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 21/03/21.
//

import Foundation

class PersonDetailsPresenter {
    
    private var view: PersonDetailsProtocol
    private var navigator: Navigator
    
    var shows: [Show?] = []
    
    init(view: PersonDetailsProtocol, navigator: Navigator) {
        self.view = view
        self.navigator = navigator
    }
    
    func getPersonInfo(personId: Int?) {
        
        guard let id = personId else { return }
        
        Network.shared.request(endpoint: "/people/" + String(id) + "/castcredits", method: .get, header: nil, parameters: ["embed": "show"]) { (result) in
            if result.isSuccess {
                do {
                    guard let data = result.value else { return }
                    let response = try JSONDecoder().decode([PersonCastCredits?].self, from: data)
                    self.shows = response.map({ (personCastCredits) -> Show? in
                        return personCastCredits?.embedded?.show
                    })
                    self.view.updateCellectionView()
                } catch {
                    print(error.localizedDescription)
                }
            } else {
                print(result.error ?? "")
            }
        }
    }
    
    func navigateToShowDetailsViewController(show: Show?) {
        guard let show = show else { return }
        navigator.navigateToShowDetailsViewController(show: show)
    }
    
}
