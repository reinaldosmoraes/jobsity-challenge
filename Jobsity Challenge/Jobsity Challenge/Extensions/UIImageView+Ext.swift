//
//  UIImageView+Ext.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 20/03/21.
//

import UIKit

extension UIImageView {
    func addDefaultGradient() {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        let startColor = UIColor(red: 30/255, green: 113/255, blue: 79/255, alpha: 0).cgColor
        let endColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1).cgColor
        gradient.colors = [startColor, endColor]
        layer.insertSublayer(gradient, at: 0)
    }
}
