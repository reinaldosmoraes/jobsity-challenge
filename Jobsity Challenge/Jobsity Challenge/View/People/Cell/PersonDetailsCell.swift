//
//  PersonDetailsCell.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 21/03/21.
//

import UIKit

class PersonDetailsCell: UITableViewCell {
    
    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var personName: UILabel!
    
}
