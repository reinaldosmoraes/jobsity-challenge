//
//  EpisodeDetailsViewController.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 20/03/21.
//

import Foundation
import UIKit
import Kingfisher

class EpisodeDetailsViewController: UIViewController {
    
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    
    var episode: Episode?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    override func viewDidLayoutSubviews() {
        posterImage.addDefaultGradient()
    }
    
    private func updateUI() {
        guard let episode = episode else { return }
        
        nameLabel.text = episode.name
        
        let description = "Season " + String(episode.season ?? 0) + " - Episode " + String(episode.number ?? 0)
        descriptionLabel.text = description
        
        if let summary = episode.summary?.htmlToString {
            summaryLabel.text = summary
        } else {
            summaryLabel.text = "No summary registered"
        }
        
        if let imageUrl = episode.image?.original {
            posterImage.kf.setImage(with: URL(string: imageUrl))
        } else {
            posterImage.image = UIImage(named: "image_placeholder")
        }
    }
}
