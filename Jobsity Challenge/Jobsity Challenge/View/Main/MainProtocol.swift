//
//  MainProtocol.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 20/03/21.
//

import Foundation

protocol MainProtocol {
    func updateTableView()
}
