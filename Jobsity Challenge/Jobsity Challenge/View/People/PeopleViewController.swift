//
//  PeopleViewController.swift
//  Jobsity Challenge
//
//  Created by Reinaldo da Silva Moraes on 21/03/21.
//

import Foundation
import UIKit

class PeopleViewController: UITableViewController {
    
    private var presenter: PeoplePresenter?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.titleView = UIView()
        presenter = PeoplePresenter(view: self, navigator: Navigator(viewContoller: self))
        configureSearchController()
    }
    
    private func configureSearchController() {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search people by name"
        navigationItem.searchController = searchController
        definesPresentationContext = true
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PersonDetailsCell", for: indexPath) as! PersonDetailsCell
        cell.personName.text = presenter?.people[indexPath.row]?.name

        if let imageUrl = presenter?.people[indexPath.row]?.image?.original {
            cell.personImage.kf.setImage(with: URL(string: imageUrl))
        } else {
            cell.personImage.image = UIImage(named: "image_placeholder")
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.people.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let person = presenter?.people[indexPath.row] {
            presenter?.navigateToPersonDetailsViewController(person: person)
        }
    }
}

extension PeopleViewController: PeopleProtocol {
    func updateTableView() {
        tableView.reloadData()
    }
}

extension PeopleViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        presenter?.getPeople(value: searchController.searchBar.text)
    }
}

